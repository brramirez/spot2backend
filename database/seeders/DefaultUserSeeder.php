<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Default User',
            'email' => 'defaultuser@example.com',
            'password' => Hash::make('password'), // Cambia la contraseña por una segura en producción
        ]);
    }
}
