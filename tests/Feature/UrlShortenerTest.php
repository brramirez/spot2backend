<?php

namespace Tests\Feature;


use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use \App\Models\Url;

class UrlShortenerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test that a URL can be shortened.
     *
     * @return void
     */
    public function test_url_empty()
    {
        // Crea un usuario de prueba
        $user = User::factory()->create();

        $response = $this->actingAs($user)->postJson('/api/urls', ['original_url' => '']);
        $response->assertStatus(400)->assertJsonStructure(['original_url']);
    }
    /**
     * Test that a URL can be shortened.
     *
     * @return void
     */
    public function test_url_can_be_shortened()
    {
        // Crea un usuario de prueba
        $user = User::factory()->create();
        // Autentica al usuario y realiza una solicitud a la ruta para crear una url
        $response = $this->actingAs($user)->postJson('/api/urls', ['original_url' => 'https://example.com']);
        // Verifica que la solicitud fue exitosa
        $response->assertStatus(201)->assertJsonStructure(['id', 'original_url', 'shortened_url']);
    }
    /**
     * Test that a shortened URL redirects to the original URL.
     *
     * @return void
     */
    public function test_shortened_url_redirects_to_original()
    {
        $url = Url::create(['original_url' => 'https://example.com', 'shortened_url' => 'abs123']);
        // Crea un usuario de prueba
        $user = User::factory()->create();
        // Autentica al usuario y realiza una solicitud a la ruta para obtener una url por su codigo url
        $response = $this->actingAs($user)->getJson('/api/urls/abs123');
        // Verifica que la solicitud fue exitosa
        $response->assertStatus(200)->assertJsonStructure(['id', 'original_url', 'shortened_url']);
    }

    public function test_get_urls()
    {
        // Crea un usuario de prueba
        $user = User::factory()->create();
        // Autentica al usuario y realiza una solicitud a la ruta para obtener las urls
        $response = $this->actingAs($user)->getJson('/api/urls');
        // Verifica que la solicitud fue exitosa
        $response->assertStatus(200)->assertJsonStructure(['current_page', 'data', 'last_page','per_page','total']);
    }

    public function test_authenticated_user_can_access_protected_route()
    {
        // Crea un usuario de prueba
        $user = User::factory()->create();

        // Autentica al usuario y realiza una solicitud a la ruta protegida
        $response = $this->actingAs($user)->get('/api/urls');

        // Verifica que la solicitud fue exitosa
        $response->assertStatus(200);
    }

    public function test_guest_user_cannot_access_protected_route()
    {
        // Realiza una solicitud a la ruta protegida sin autenticación
        $response = $this->getJson('/api/urls');

        // Verifica que la solicitud retorne un estado Unauthorized para ser manejado desde el front
        $response->assertStatus(401)->assertJsonStructure(['message']);
    }

    public function test_authenticated_user_can_delete_url()
    {
        // Crear un usuario y autenticarlo
        $user = User::factory()->create();


        // Crear una URL
        $url = Url::create([
            'original_url' => 'https://example.com',
            'shortened_url' => 'short123',
        ]);

        // Hacer una solicitud DELETE para eliminar la URL
        $response = $this->actingAs($user)->deleteJson('/api/urls/' . $url->shortened_url);

        // Verificar que la URL ha sido eliminada
        $response->assertStatus(200)->assertJsonStructure(['message']);
        $this->assertDatabaseMissing('urls', ['shortened_url' => 'short123']);
    }
}
