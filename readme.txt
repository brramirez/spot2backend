# URL Shortener

## Development steps
## Este proyecto es un acortador de URL desarrollado con Laravel. La aplicación incluye la documentación de la API, pruebas unitarias e integración, y medidas de seguridad para prevenir inyección SQL, ataques CSRF y XSS, y asegurar la escalabilidad y rendimiento.

# Requisitos
## PHP >= 7.4
## Composer
## MySQL o cualquier base de datos soportada por Laravel

## Instalación

1. Run composer install
2. Run cp .env.example .env
3. Run php artisan key:generate
4. Make the respective changes to the environment variables.
5. Run php artisan migrate --seed
6. Run php artisan test

# Despliegue

# Seguridad
## Este proyecto incluye medidas para prevenir:
## Inyección de SQL: Usando consultas preparadas y el Query Builder de Laravel.
## Ataques CSRF: Usando los tokens CSRF generados por Laravel.
## Ataques XSS: Escapando adecuadamente la salida de datos.
