<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUrlApiRequest;
use App\Models\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UrlController extends Controller
{

    /**
     * @OA\Post(
     *     path="/api/urls",
     *     summary="Create a shortened URL",
     *     tags={"URLs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"original_url"},
     *             @OA\Property(property="original_url", type="string", format="url", example="https://example.com")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="URL created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example=1),
     *             @OA\Property(property="original_url", type="string", example="https://example.com"),
     *             @OA\Property(property="shortened_url", type="string", example="abc123")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="original_url", type="array", @OA\Items(type="string", example="The original url field is required."))
     *         )
     *     )
     * )
     */
    public function store(CreateUrlApiRequest $request)
    {

        $shortened_url = $this->generateUniqueShortenedUrl();

        $url = Url::create([
            'original_url' => $request->original_url,
            'shortened_url' => $shortened_url
        ]);

        return response()->json($url, 201);

    }

    /**
     * @OA\Get(
     *     path="/api/urls",
     *     summary="Get a list of shortened URLs",
     *     tags={"URLs"},
     *     @OA\Response(
     *         response=200,
     *         description="List of URLs",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="data", type="array", @OA\Items(
     *                 @OA\Property(property="id", type="integer", example=1),
     *                 @OA\Property(property="original_url", type="string", example="https://example.com"),
     *                 @OA\Property(property="shortened_url", type="string", example="abc123")
     *             )),
     *             @OA\Property(property="links", type="object"),
     *             @OA\Property(property="meta", type="object")
     *         )
     *     )
     * )
     */
    public function index()
    {

        $urls = Url::paginate(10);
        return response()->json($urls, 200);
    }


    /**
     * @OA\Get(
     *     path="/api/urls/{shortened_url}",
     *     summary="Get details of a specific shortened URL",
     *     tags={"URLs"},
     *     @OA\Parameter(
     *         name="shortened_url",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string"),
     *         description="The shortened URL code"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="URL details",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example=1),
     *             @OA\Property(property="original_url", type="string", example="https://example.com"),
     *             @OA\Property(property="shortened_url", type="string", example="abc123")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="URL not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="No query results for model [App\\Models\\Url].")
     *         )
     *     )
     * )
     */
    public function show($shortened_url)
    {

        $url = Url::where('shortened_url', $shortened_url)->firstOrFail();
        return response()->json($url, 200);
    }

    private function generateUniqueShortenedUrl()
    {
        $shortened_url = Str::random(6);

        while (Url::where('shortened_url', $shortened_url)->exists()) {
            // Genera una cadena de 6 caracteres usando bytes aleatorios criptográficamente seguros
            $shortened_url = bin2hex(random_bytes(3));
        }

        return $shortened_url;
    }

    /**
     * @OA\Delete(
     *     path="/api/urls/{shortened_url}",
     *     summary="Delete a shortened URL",
     *     tags={"URLs"},
     *     security={{ "sanctum": {} }},
     *     @OA\Parameter(
     *         name="shortened_url",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="URL deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="URL deleted successfully")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="URL not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="URL not found")
     *         )
     *     )
     * )
     */
    public function destroy($shortened_url)
    {
        $url = Url::where('shortened_url', $shortened_url)->first();

        if (!$url) {
            return response()->json(['message' => 'URL not found'], 404);
        }

        $url->delete();

        return response()->json(['message' => 'URL deleted successfully'], 200);
    }

}
